package main

import (
	"flag"
	"fmt"
	"github.com/pkg/errors"
	"os"
)

type argsStruct struct {
	keepenv bool
	dir string
	executable string
}

func newArgs() *argsStruct {
	args := &argsStruct{}
	flag.BoolVar(&args.keepenv, "keepenv", false, "keep environment")
	return args
}

func (a *argsStruct) parseArgs() error {
	flag.Parse()
	if flag.NArg() != 2 {
		return errors.New("Exactly two positional arguments required")
	}
	a.dir, a.executable = flag.Arg(0), flag.Arg(1)
	return nil
}

func usage() {
	_, _ = fmt.Fprintf(os.Stderr, "Usage of %s:\n\n", os.Args[0])
	_, _ = fmt.Fprintf(os.Stderr, "  %s [-keepenv] dir executable\n\n", os.Args[0])
	flag.PrintDefaults()
}

