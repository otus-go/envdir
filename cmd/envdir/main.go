package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
)

func main() {

	log.SetFlags(0)
	flag.Usage = usage

	// init and parse flags
	args := newArgs()
	err := args.parseArgs()
	if err != nil {
		flag.Usage()
		os.Exit(1)
	}

	// read environment variables from directory
	environ, err := getEnviron(args.dir)
	if err != nil {
		log.Fatalln(err)
	}

	// execute command
	err = runCmd(args.executable, environ, args.keepenv)
	if err != nil {
		log.Fatalln(err)
	}
}


func getEnviron(dir string) ([]string, error) {

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	environ := make([]string, 0, len(files))
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		fullPath := path.Join(dir, file.Name())
		value, err := ioutil.ReadFile(fullPath)
		if err != nil {
			return nil, err
		}
		environ = append(environ, fmt.Sprintf("%s=%s", file.Name(), value))
	}
	return environ, nil
}


func runCmd(executable string, environ []string, keepenv bool) error {
	cmd := exec.Command(executable)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if keepenv {
		cmd.Env = append(os.Environ(), environ...)
	} else {
		cmd.Env = environ
	}
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}
